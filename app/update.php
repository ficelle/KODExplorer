<?php

class updateKod{
	public function __construct(){
		del_file(__FILE__);
		$this->run('version114','1.14');
	}
	
	// 更新前版本低于$version时执行更新脚本; 
	public function run($method,$version){
		Model('SystemOption')->cacheRemove('');
		$beforeVersion = floatval(Model('SystemOption')->get('currentVersion'));
		write_log(array('update start:'.$version,$GLOBALS['in'],get_caller_msg()),'task');
		Model('SystemOption')->set('currentVersion',$version);
		if($beforeVersion >= floatval($version)) return;
		
		$this->$method();
		write_log('update end:'.$version,'task');
	}
	// 执行sql更新;
	public function runSql($version){
		del_dir(TEMP_PATH.'_fields');
		$dbType = getDatabaseType();
		$path  	= BASIC_PATH."app/controller/install/data/update$version/$dbType.sql";
		$exist 	= file_exists($path);
		write_log('update runSql:'.$path.';exist='.intval($exist),'task');
		if(!$exist) return;
		
		$sqlArr = sqlSplit(file_get_contents($path));
		write_log(array('update sql:',$sqlArr),'task');
		foreach($sqlArr as $sql){
			$result = Model()->db()->execute($sql);
			write_log('update sql: res='.$result.';sql='.$sql,'task');
		}
		del_dir(TEMP_PATH.'_fields');
	}

	// 升级到1.14; 不能退出;
	public function version114(){
		$this->runSql("1.14");
		Action('admin.repair')->resetShareTo();
	}
}
new updateKod();